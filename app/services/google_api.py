import copy
from typing import List
from datetime import datetime

from aiogoogle import Aiogoogle

from app.utils.constans import FORMAT, SPREADSHEET_TEMPLATE, TABLE_VALUES_TEMPLATE
from app.core.config import settings
from app.schemas.charity_project import CharityProjectDB


def create_spreadsheet_json(date_time: str) -> dict:
    spreadsheet_body = copy.deepcopy(SPREADSHEET_TEMPLATE)
    spreadsheet_body['properties']['title'] = f'Отчет на {date_time}'
    return spreadsheet_body


def create_table_values(date_time: str) -> list:
    table_values = copy.deepcopy(TABLE_VALUES_TEMPLATE)
    table_values[0].append(date_time)
    return table_values


async def spreadsheets_create(wrapper_services: Aiogoogle) -> str:
    '''Создаёт гугл-таблицу с отчётом на сервисном аккаунте.'''
    now_date_time = datetime.now().strftime(FORMAT)
    service = await wrapper_services.discover('sheets', 'v4')
    spreadsheet_body = create_spreadsheet_json(now_date_time)
    response = await wrapper_services.as_service_account(
        service.spreadsheets.create(json=spreadsheet_body)
    )
    spreadsheetid = response['spreadsheetId']
    return spreadsheetid  # noqa


async def set_user_permissions(
        spreadsheetid: str,
        wrapper_services: Aiogoogle
) -> None:
    '''Выдаёт права личному аккаунту к документам созданным с сервисного аккаунта.'''
    permissions_body = {'type': 'user',
                        'role': 'writer',
                        'emailAddress': settings.email}
    service = await wrapper_services.discover('drive', 'v3')
    await wrapper_services.as_service_account(
        service.permissions.create(
            fileId=spreadsheetid,
            json=permissions_body,
            fields="id"
        ))


async def spreadsheets_update_value(
    spreadsheetid: str,
    projects: List[CharityProjectDB],
    wrapper_services: Aiogoogle
):
    '''Обновляет данные в гугл-таблице.'''
    now_date_time = datetime.now().strftime(FORMAT)
    service = await wrapper_services.discover('sheets', 'v4')
    table_values = create_table_values(now_date_time)
    for project in projects:
        sort_projects = project['close_date'] - project['create_date']
        new_row = [
            str(project['name']),
            str(sort_projects),
            str(project['description'])
        ]
        if len(new_row) != len(table_values[0]):
            raise ValueError("Ошибка: передаваемые данные не поместятся в таблице")
            # new_row = new_row[:len(table_values[0])]
        else:
            table_values.append(new_row)

    update_body = {
        'majorDimension': 'ROWS',
        'values': table_values,
    }
    await wrapper_services.as_service_account(
        service.spreadsheets.values.update(
            spreadsheetId=spreadsheetid,
            range=fr'A1:C{len(table_values)}',
            valueInputOption='USER_ENTERED',
            json=update_body
        )
    )
