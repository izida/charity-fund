DEFAULT_INVESTED_AMOUNT = 0
DEFAULT_FULLY_INVESTED = False
MAX_LEN_STRING = 100
MIN_ANYSTR_LENGTH = 1
FORMAT = "%Y/%m/%d %H:%M:%S"
TABLE_VALUES_TEMPLATE = [
    ['Отчет от', 'now_date_time'],
    ['Топ проектов по скорости закрытия'],
    ['Название проекта', 'Время сбора', 'Описание']
]
SPREADSHEET_TEMPLATE = {
    'properties': {
        'title': '',
        'locale': 'ru_RU'
    },
    'sheets': [{
        'properties': {
            'sheetType': 'GRID',
            'sheetId': 0,
            'title': 'Лист1',
            'gridProperties': {
                'rowCount': 100,
                'columnCount': 11
            }
        }
    }]
}